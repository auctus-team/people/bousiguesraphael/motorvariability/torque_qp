[![pipeline status](https://gitlab.inria.fr/auctus/panda/torque_qp/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/torque_qp)
[![Quality Gate Status](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=auctus%3Apanda%3Atorque-qp&metric=alert_status)](https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Atorque-qp)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=auctus%3Apanda%3Atorque-qp&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Atorque-qp)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Atorque-qp
- Documentation : https://auctus.gitlabpages.inria.fr/panda/torque_qp/index.html

# Torque QP

A generic low-level joint torque controller with a QP formulation. It's implemation has been done on the Franka Emika Panda Robot. The software includes a simulation part on gazebo and and integration with the franka_ros packages.

To learn more about this project, read the [wiki](https://gitlab.inria.fr/auctus-team/people/bousiguesraphael/public/motorvariability/torque_qp/-/wikis/home). 
